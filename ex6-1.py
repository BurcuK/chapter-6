import sys

def test(did_pass):
    """  Print the result of a test.  """
    linenum = sys._getframe(1).f_lineno   # Get the caller's line number.
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def turn_clockwise(n):
	N="N"
	E="E"
	S="S"
	W="W"

	if n==N:
		return E
	elif n==E:
		return E
	elif n==S:
		return W
	elif n==W:
		return N
	else:
		return None
	
def test_clock():
	test(turn_clockwise("N")=="E")
	test(turn_clockwise("E")=="S")
	test(turn_clockwise("S")=="W")
	test(turn_clockwise("W")=="N")


test_clock()
