import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def day_num(n):
	if n=="Sunday":
		return 0
	elif n=="Monday":
		return 1
	elif n=="Tuesday":
		return 2
	elif n=="Wednesday":
		return 3
	elif n=="Thursday":
		return 4
	elif n=="Friday":
		return 5
	elif n=="Saturday":
		return 6
	else:
		return None
	

def test_day():
	test(day_num("Sunday")==0)
	test(day_num("Monday")==1)
	test(day_num("Tuesday")==2)
	test(day_num("Wednesday")==3)
	test(day_num("Thursday")==4)
	test(day_num("Friday")==5)
	test(day_num("Saturday")==6)
	test(day_num("Halloween") == None)

test_day()
