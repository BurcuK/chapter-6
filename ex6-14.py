import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def is_even(n):
	if n%2==0:
		return True
	elif n%2!=0:
		return False

def is_odd(n):
	if n%2==0:
		return False
	elif n%2!=0:
		return True


def test_evenOdd():
	test(is_even(3)==False)
	test(is_odd(21)==True)
	test(is_even(6)==True)

test_evenOdd()
	
