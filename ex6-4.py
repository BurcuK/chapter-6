import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def day_add(n,i):

	n=int(n)
	a=(n+i)%7
	if a==0:
		return "Sunday"
	elif a==1:
		return "Monday"
	elif a==2:
		return "Tuesday"
	elif a==3:
		return "Wednesday"
	elif a==4:
		return "Thursday"
	elif a==5:
		return "Friday"
	elif a==6:
		return "Saturday"
			
def test_day():
	test(day_add("Sunday", 3)=="Wednesday")
	test(day_add("Friday", 3)=="Monday")
	test(day_add("Saturday", 5)=="Thursday")
	test(day_add("Wednesday", 2)=="Friday")

test_day()
