import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def days_in_month(n):
	if n=="January":
		return 31
	elif n=="February":
		return 28
	elif n=="March":
		return 31
	elif n=="April":
		return 30
	elif n=="May":
		return 31
	elif n=="June":
		return 30
	elif n=="July":
		return 31
	elif n=="August":
		return 31
	elif n=="September":
		return 30
	elif n=="October":
		return 31
	elif n=="November":
		return 30
	elif n=="December":
		return 31
	
	

def test_month():
	test(days_in_month("February") == 28)
	test(days_in_month("December") == 31)
	test(days_in_month("November") == 30)
	test(days_in_month("April") == 30)
	

test_month()
