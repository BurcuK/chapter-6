import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def compare(n,m):
	if n>m:
		return 1
	elif n==m:
		return 0
	elif n<m:
		return -1

def test_com():
	test(compare(2,3)==-1)
	test(compare(4,4)==0)
	test(compare(6,3)==1)

test_com()
