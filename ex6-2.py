import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def day_name(n):
	if n==0:
		return "Sunday"
	elif n==1:
		return "Monday"
	elif n==2:
		return "Tuesday"
	elif n==3:
		return "Wednesday"
	elif n==4:
		return "Thursday"
	elif n==5:
		return "Friday"
	elif n==6:
		return "Saturday"
	else:
		return None

def test_day():
	test(day_name(0)=="Sunday")
	test(day_name(1)=="Monday")
	test(day_name(2)=="Sunday")
	test(day_name(3)=="Wednesday")
	test(day_name(4)=="Thursday")
	test(day_name(5)=="Friday")
	test(day_name(6)=="Saturday")

test_day()
