import sys
import math

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def hypotenuse(a,b):
	n=(a*a)+(b*b)
	return float(math.sqrt(n))

def test_hypo():
	test(hypotenuse(3, 4) == 5.0)
	test(hypotenuse(12, 5) == 13.0)
	test(hypotenuse(24, 7) == 25.0)
	test(hypotenuse(9, 12) == 15.0)

test_hypo()
	
