import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def to_secs(i,j,k):
	i=float(i)
	j=float(j)
	k=float(k)

	i=i*60*60
	j=j*60
	
	return int(i+j+k)

def test_sec():
	test(to_secs(2.5, 0, 10.71) == 9010)
	test(to_secs(2.433,0,0) == 8758)
	test(to_secs(1,1,-1) == -3661)
	

test_sec()
