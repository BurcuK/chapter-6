import sys

def test(did_pass):
    linenum = sys._getframe(1).f_lineno 
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)

def hours_in(n):
	return int(n/3600)

def minutes_in(n):
	return int(n/60)

def seconds_in(n):
	return int(n)

def test_sec():
	test(hours_in(9010) == 2)
	test(minutes_in(9010) == 150)
	test(seconds_in(9010) == 10)

test_sec()
